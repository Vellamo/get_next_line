# GET NEXT LINE


## Summary
The purpose of this 42 Project was to create a function that, when called, will read until /n or EOF. 
It should handle errors and not leak any memory.
As a bonus, it should be able to handle multiple file descriptors.

## Usage
To run, GNL (Get Next Line) needs the existing Libft (included). You can either compile a new Libft.a that also includes GNL, OR write a main that includes both libft.a and GNL.c.

• GNL is defined as taking a file descriptor (fd) and **char. 
• The return value can be 1, 0 or -1 depending on whether a line has been read, when the reading has been completed, or if an error has happened respectively.
• GNL essentially moves the pointer (of **char) from line (\n), to the next. 

https://gitlab.com/Vellamo/gnl_sanstester 
Contains a rudimentary tester I created, that can show some of the functions capabilities.

### Example Output
The GNL program itself will return a 0 or 1, depending on whether the EOF was encountered. This behaviour utilises Read's functionality. 