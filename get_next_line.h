/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.h                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lharvey <lharvey@student.hive.fi>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/12 10:00:18 by lharvey           #+#    #+#             */
/*   Updated: 2019/11/29 09:12:41 by lharvey          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef GET_NEXT_LINE_H
# define GET_NEXT_LINE_H

int	get_next_line (const int fd, char **line);
# define BUFF_SIZE 1
# define FD_SIZE 4864
#include "libft/libft.h"
#include <unistd.h>
#include <stdlib.h>

#endif
